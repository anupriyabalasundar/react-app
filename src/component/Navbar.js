import './Navbar.css'
import React,{useState} from 'react'

import {Link} from 'react-router-dom'
import {FaBars,FaTimes} from 'react-icons/fa'
import logo from './logo.png'


function Navbar(){
    const[icon,setIcon]=useState(false)
    const handleClick=()=>{
        setIcon(!icon)
    }
    const closeSideDrawer=()=>{
        setIcon(false)
    }
    return(
        <>
            <nav className='navbar'>
                <Link to='/' className='nav-logo' onClick={closeSideDrawer}><img class="image" src={logo} alt="logo" width={110} height={110}></img></Link>
                <div className='menu-icon' onClick={handleClick}>
                    {
                        icon ?<FaTimes className='fa-times'></FaTimes> : <FaBars className='fa-bars'></FaBars>
                    }
                </div>
                <ul className={icon?'nav-menu active':'nav-menu'}>
                    <li>
                        <Link to='/' className='nav-links' onClick={closeSideDrawer}>Home</Link>
                    </li>
                    <li>
                        <Link to='/About' className='nav-links' onClick={closeSideDrawer}>About</Link>
                    </li>
                    <li>
                        <Link to='/My' className='nav-links' onClick={closeSideDrawer}>Dashboard</Link>
                    </li>
                    <li>
                        <Link to='/login' className='nav-links' onClick={closeSideDrawer}>Login IN</Link>
                    </li>
                    <li>
                        <Link to='/Allcourses' className='nav-links' onClick={closeSideDrawer}>All Courses</Link>
                    </li>
                    <li>
                        <Link to='/Signup' className='nav-links' onClick={closeSideDrawer}>Signup</Link> 
                    </li>
                </ul>
            </nav>
        </>
    )
}
export default Navbar;