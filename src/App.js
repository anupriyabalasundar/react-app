import React from "react";
import "./App.css";
import {BrowserRouter as Router,Switch,Route}from 'react-router-dom';
import Navbar from "./component/Navbar";
import Home from './pages/Home'
import About from "./pages/About";
import My from "./pages/My";
import login from "./pages/login";
import Signup from "./pages/Signup";
import Allcourses from "./pages/Allcourses";
import node from "./courses/node"
import devops from "./courses/devops"
import github from "./courses/github"
import html from "./courses/html"
import java from "./courses/java"
import python from "./courses/python"
import react from "./courses/react"
import sdlc from "./courses/sdlc"



function App(){
     
    return (
        
            
        <Router>
            <Navbar/>
                <Switch>
                    <Route path='/' exact component><Home/></Route>
                        <Route path='/About' ><About/></Route>
                        <Route path='/My' component={My}/>
                        <Route path='/login' component={login}/>
                        <Route path='/Allcourses' component={Allcourses}/>
                        <Route path='/Signup' component={Signup}/>
                        <Route path='/node' component={node}/>
                        <Route path='/devops' component={devops}/>
                        <Route path='/github' component={github}/>
                        <Route path='/html' component={html}/>
                        <Route path='/java' component={java}/>
                        <Route path='/python' component={python}/>
                        <Route path='/react' component={react}/>
                        <Route path='/sdlc' component={sdlc}/>
                </Switch>
        </Router>
        
         )
}
export default App