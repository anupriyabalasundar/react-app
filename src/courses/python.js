import React from "react";
function python(){
    return(
        <div>
            <h1>Python Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>Python is a high-level, interpreted, general-purpose programming language. Its design philosophy emphasizes code readability with the use of significant indentation.[30]

Python is dynamically-typed and garbage-collected. It supports multiple programming paradigms, including structured (particularly procedural), object-oriented and functional programming. It is often described as a "batteries included" language due to its comprehensive standard library</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default python;