import React from "react";
import "./courses.css";
function devops(){
    return(
        <div >
            <h1>Devops Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>DevOps is a set of practices that combines software development (Dev) and IT operations (Ops). It aims to shorten the systems development life cycle and provide continuous delivery with high software quality.[1] DevOps is complementary with Agile software development; several DevOps aspects came from the Agile methodology.</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default devops;