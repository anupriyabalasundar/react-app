import React from "react";
function github(){
    return(
        <div >
            <h1>Github Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>Every repository on GitHub.com comes equipped with a section for hosting documentation, called a wiki. You can use your repository's wiki to share long-form content about your project, such as how to use it, how you designed it, or its core principles. A README file quickly tells what your project can do, while you can use a wiki to provide additional documentation. For more information, see "About READMEs."</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default github;