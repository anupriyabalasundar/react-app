import React from "react";
function react(){
    return(
        <div>
           <h1>React Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>React (also known as React.js or ReactJS) is a free and open-source front-end JavaScript library[3] for building user interfaces based on UI components. It is maintained by Meta (formerly Facebook) and a community of individual developers and companies.[4][5][6] React can be used as a base in the development of single-page, mobile, or server-rendered applications with frameworks like Next.js. However, React is only concerned with state management and rendering that state to the DOM, so creating React applications usually requires the use of additional libraries for routing, as well as certain client-side functionality.</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default react;