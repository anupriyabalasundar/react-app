import React from "react";
function sdlc(){
    return(
        <div >
            <h1>SDLC Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>In systems engineering, information systems and software engineering, the systems development life cycle (SDLC), also referred to as the application development life-cycle, is a process for planning, creating, testing, and deploying an information system.[1] The systems development life cycle concept applies to a range of hardware and software configurations, as a system can be composed of hardware only, software only, or a combination of both.[2] There are usually six stages in this cycle: requirement analysis, design, development and testing, implementation, documentation, and evaluation.

</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default sdlc;