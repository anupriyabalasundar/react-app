import React from "react";
function node(){
    return(
        <div>
           <h1>Node Basics</h1>
            <p>Course start:19-12-2022</p>
            <p>Course end:15-06-2022</p>
            <p><br></br>Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser. Node.js lets developers use JavaScript to write command line tools and for server-side scripting—running scripts server-side to produce dynamic web page content before the page is sent to the user's web browser. Consequently, Node.js represents a "JavaScript everywhere" paradigm,[6] unifying web-application development around a single programming language, rather than different languages for server-side and client-side scripts.</p>
            <button class="btn">Enroll Now</button>
        </div>
    )
}export default node;