import React from "react";
import { Link } from "react-router-dom";
import coding from './coding.jpg'
import reactjs from './reactjs.jpg'
 function AllCourses(){
     return(
         
         <div>
             <div class="column">
                
                     <img class="image" src={reactjs} alt="node" width={250} height={250}></img>
                        <div class="container">
                        <Link to='/node' className='link' >node</Link>
                            
                        </div>
                
            
                     <img class="image" src={reactjs} alt="devops" width={250} height={250}></img>
                        <div class="container">
                        <Link to='/devops' className='link' >Devops</Link>
                        </div>
                
                
                     <img class="image" src={reactjs} alt="github" width={250} height={250}></img>
                        <div class="container">
                        <Link to='/github' className='link' >Github</Link>
                        </div>
               
                
                     <img class="image" src={reactjs} alt="html" width={250} height={250}></img>
                        <div class="container">
                        <Link to='/html' className='link' >HTML</Link>
                        </div>
                

             </div>
             <div class="row">
             
                    
                        <img class="image" src={coding} alt="react" width={250} height={250} ></img>
                        <div class="container">
                        <Link to='/react' className='link' >React</Link>
                    </div>
             
             
                    
                        <img class="image" src={coding} alt="Java" width={250} height={250} ></img>
                        <div class="container">
                        <Link to='/java' className='link' >Java</Link>
                    </div>
             
            
                    
                        <img class="image" src={coding} alt="python" width={250} height={250} ></img>
                        <div class="container">
                        <Link to='/python' className='link' >Python</Link>
                    </div>
            
             
                    
                        
            
             </div>
             
         </div>
     )
 }
 export default AllCourses;